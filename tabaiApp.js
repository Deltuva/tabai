var Tabai = {

    addLoadEvent: function (fn) {
        var x = window.onload;
        if (x) {
            window.onload = (x && typeof x == 'function') ? function () {
                x();
                fn()
            } : fn;
        }
    },

    registerHelper: function () {
        Handlebars.registerHelper('if', function (conditional, options) {
            if (conditional) {
                return options.fn(this);
            }
        });
    },

    callBack: function (data) {
        jData = data;
        Tabai.compileAndDisplayTemplate();
    },

    compileAndDisplayTemplate: function () {
        var source = $("#tabaiTemplate").html();
        if (source !== null) {
            var template = Handlebars.compile(source);
            var html = template({
                tabai: jData.data
            });
            //console.log(html);

            $("#content").append(html);
        }
    },

    build: function () {
        $.getJSON('data.json', Tabai.callBack);
    }
}

Tabai.addLoadEvent(Tabai.registerHelper());
Tabai.addLoadEvent(Tabai.build());
